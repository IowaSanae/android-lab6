const pizzaId = 'id';
const pizzaName = 'pizzaName';
const pizzaDescription = 'description';
const pizzaPrice = 'price';
const pizzaImage = 'imageUrl';

class Pizza {
  late int id;
  late String pizzaName;
  late String description;
  late double price;
  late String imageUrl;

  Pizza.fromJson(Map<String, dynamic> json) {
    this.id = json['id'] ?? 0;
    this.pizzaName = json[pizzaName] ?? '';
    this.description = json[pizzaDescription] ?? '';
    this.price = json[pizzaPrice] ?? 0.0;
    this.imageUrl = json[pizzaImage] ?? '';
  }
}