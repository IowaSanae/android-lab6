import 'package:flutter/material.dart';
import './views/pizza_app.dart';

void main() {
  runApp(const PizzaApp());
}
